import './App.css';
import {
  Routes,
  Route,
} from "react-router-dom";
import HeaderComponent from './components/HeaderComponent'
import FooterComponent from './components/FooterComponent'
import DashboardComponent from './components/DashboardComponent'
import CarDetailComponent from './components/CarDetailComponent'
import PaymentComponent from './components/PaymentComponent'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { setCars, fetchCars } from './redux/actions/carActions'
import React, { useEffect } from 'react'
function App() {
  // const cars = useSelector(state => state.allCars.cars)
  const dispatch = useDispatch()

  // const fetchCars = async () => {
  //   const response = await axios.get("https://625bcc2d50128c57020785c4.mockapi.io/binarcar/mobil")
  //     .catch(err => console.log('error: ', err))
  //   dispatch(setCars(response.data))
  // }

  useEffect(() => {
    dispatch(fetchCars())
  }, [])
  return (
    <div className="App">
      <HeaderComponent />
      <Routes>
        <Route path='/' element={<DashboardComponent />} />
        <Route path='/car-detail/:id' element={<CarDetailComponent />} />
        <Route path='/payment/' element={<PaymentComponent />} />
      </Routes>
      <FooterComponent />
    </div>
  );
}

export default App;
