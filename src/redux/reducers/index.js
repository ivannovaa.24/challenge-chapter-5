import { combineReducers } from 'redux'
import { carsReducer, carReducer, btnReducer } from './carReducer'

const reducers = combineReducers({
  allCars: carsReducer,
  carDetail: carReducer,
  button: btnReducer
})

export default reducers