import React from 'react';
import { Box, Flex, Image, FormControl, FormLabel, Input, Button, Heading } from '@chakra-ui/react';
import login from '../images/login_img.png'
import car from '../images/car.png'
import { Link } from "react-router-dom"
const LoginComponent = () => {
  return (
    <Flex>
      <Box w={
        {
          base: '100%',
          md: '70%',
        }
      }>
        <Image
          src={login}
          h='100vh' w='100vw'
        />
      </Box>
      <Flex justifyContent='center' align='center'
        w={
          {
            base: '100%',
            md: '30%'
          }
        }
        pos={{
          base: 'absolute',
          md: 'static'
        }}
        top='0' bottom='0' left='0' right='0' margin='auto'>
        <Box borderWidth='1px' p='5' borderRadius='md' background='white'>
          <FormControl>
            <Image
              src={car}
              h='100px'
            />
            <Heading as='h6' size='md' my='3' isTruncated>Welcome to Binar</Heading>
            <FormLabel htmlFor='email'>Email address</FormLabel>
            <Input id='email' type='email' mb='3' />
            <FormLabel htmlFor='password'>Password</FormLabel>
            <Input id='password' type='password' mb='5' />
            <Link to='/dashboard'>
              <Button w='100%' colorScheme='blue'>Login</Button>
            </Link>
          </FormControl>
        </Box>
      </Flex>
    </Flex>
  )
}

export default LoginComponent